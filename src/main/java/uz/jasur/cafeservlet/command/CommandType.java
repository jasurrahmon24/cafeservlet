package uz.jasur.cafeservlet.command;

import uz.jasur.cafeservlet.command.impl.*;

public enum CommandType {
    LOGIN(new LoginCommand()),
    REGISTER(new RegisterCommand()),
    UPDATE_USER(new UpdateUserCommand()),
    MAKE_ORDER(new MakeOrderCommand()),
    CHANGE_STATUSES(new ChangeStatusesCommand()),
    MAKE_COMMENT(new MakeCommentCommand()),
    TRANSFER(new TransferCommand()),
    ADD_MEAL(new AddMealCommand()),
    EDIT_MEAL(new EditMealCommand()),
    CHANGE_USER_STATUS(new ChangeUserStatusCommand()),
    LOGOUT(new LogoutCommand());
    Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public static Command define(String commandStr) {
        CommandType current = CommandType.valueOf(commandStr.toUpperCase());
        return current.command;
    }
}
