package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.MealDaoImpl;
import uz.jasur.cafeservlet.entity.Meal;

import java.io.File;
import java.io.IOException;

public class AddMealCommand implements Command {
    private static final String UPLOAD_DIR = "img";
    @Override
    public String execute(HttpServletRequest request) {
        String mealName = request.getParameter("mealName");
        double mealPrice = Double.parseDouble(request.getParameter("mealPrice"));
        String mealIngredient = request.getParameter("mealIngredient");
        double mealRate = Double.parseDouble(request.getParameter("mealRate"));
        String mealDescription = request.getParameter("mealDescription");

        String applicationPath = request.getServletContext().getRealPath("");
        String uploadFilePath = applicationPath + UPLOAD_DIR;
        File fileSaveDir = new File(uploadFilePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        Part filePart = null;
        String fileName = null;
        try {
            filePart = request.getPart("imageSrc");
            fileName = filePart.getSubmittedFileName();
            filePart.write(uploadFilePath + File.separator + fileName);
        } catch (IOException | ServletException e) {
            throw new RuntimeException(e);
        }
        String filePath = File.separator + UPLOAD_DIR + File.separator + fileName;
        Meal meal = new Meal(mealName, mealPrice, mealDescription, mealRate, mealIngredient, filePath);
        MealDaoImpl mealDao = new MealDaoImpl();
        mealDao.insert(meal);
        return "pages/admin.jsp";
    }
}
