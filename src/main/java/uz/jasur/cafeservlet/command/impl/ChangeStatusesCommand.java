package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.OrderDaoImpl;
import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.enums.OrderStatus;
import uz.jasur.cafeservlet.entity.enums.PayStatus;

public class ChangeStatusesCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        String payStatus = request.getParameter("payStatus");
        String orderStatus = request.getParameter("orderStatus");
        Order order = new Order(orderId, OrderStatus.valueOf(orderStatus.toUpperCase()), PayStatus.valueOf(payStatus.toUpperCase()));
        OrderDaoImpl orderDao = new OrderDaoImpl();
        if(orderDao.update(order) != null){
            request.setAttribute("order_status_msg", "Statuses changed successfully");
        } else {
            request.setAttribute("order_status_msg", "Statuses cannot be changed");
        }
        return "pages/admin.jsp";
    }
}
