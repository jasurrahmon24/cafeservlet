package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.entity.enums.UserStatus;
import uz.jasur.cafeservlet.service.UserService;
import uz.jasur.cafeservlet.service.impl.UserServiceImpl;

public class ChangeUserStatusCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String userId = request.getParameter("userIdFromUsers");
        String status = request.getParameter("userStatusFromUsers");
        System.out.println("userId: " + userId);
        System.out.println("Status: " + status);
        User user = new User();
        user.setUserStatus(UserStatus.valueOf(status.toUpperCase()));
        user.setId(Integer.parseInt(userId));
        UserService userService = UserServiceImpl.getInstance();
        if(userService.updateStatus(user)){
            request.setAttribute("user_status_msg", "Updated successfully");
        } else {
            request.setAttribute("user_status_msg", "Cannot be updated");
        }
        return "pages/admin.jsp";
    }
}
