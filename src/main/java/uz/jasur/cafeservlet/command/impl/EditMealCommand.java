package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.MealDaoImpl;
import uz.jasur.cafeservlet.entity.Meal;

public class EditMealCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String mealName = request.getParameter("mealName");
        double mealPrice = Double.parseDouble(request.getParameter("mealPrice"));
        String mealIngredient = request.getParameter("mealIngredient");
        String mealDescription = request.getParameter("mealDescription");
        int mealId = Integer.parseInt(request.getParameter("mealId"));
        Meal meal = new Meal();
        meal.setName(mealName);
        meal.setPrice(mealPrice);
        meal.setIngredients(mealIngredient);
        meal.setDescription(mealDescription);
        meal.setId(mealId);
        MealDaoImpl mealDao = new MealDaoImpl();
        if(mealDao.update(meal) != null){
            return "pages/admin.jsp";
        }
        return null;
    }
}
