package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.MealDaoImpl;
import uz.jasur.cafeservlet.entity.Meal;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.service.UserService;
import uz.jasur.cafeservlet.service.impl.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class LoginCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        UserService userService = UserServiceImpl.getInstance();
        String page;
        User user = userService.authenticate(login, password);
        if(user != null){
            request.setAttribute("user", login);
            request.getSession().setAttribute("auth", user);
            MealDaoImpl mealDao = new MealDaoImpl();
            List<Meal> meals = mealDao.findAll();
            request.setAttribute("meals", meals);
            page = user.getUserRole().getRole().equals("admin") ? "pages/admin.jsp" : "pages/user.jsp";
        } else {
            request.setAttribute("login_msg", "Incorrect login or password");
            page = "pages/login.jsp";
        }

        return page;
    }
}
