package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.CommentDaoImpl;
import uz.jasur.cafeservlet.entity.Comment;

public class MakeCommentCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        int mealId = Integer.parseInt(request.getParameter("mealId"));
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        String commentText = request.getParameter("commentText");
        int commentRate = Integer.parseInt(request.getParameter("commentRate"));
        Comment dto = new Comment(mealId, orderId, commentText, commentRate);
        CommentDaoImpl dao = new CommentDaoImpl();
        if (dao.insert(dto)) {
            request.setAttribute("message", "Your comment is saved.");
        } else {
            request.setAttribute("message", "Your comment cannot be saved.");
        }
        return "pages/mealPage.jsp?id=" + mealId;
    }
}
