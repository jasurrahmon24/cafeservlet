package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.BalanceDaoImpl;
import uz.jasur.cafeservlet.dao.impl.MealDaoImpl;
import uz.jasur.cafeservlet.dao.impl.OrderDaoImpl;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.entity.Meal;
import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.entity.enums.OrderStatus;
import uz.jasur.cafeservlet.entity.enums.PayStatus;
import uz.jasur.cafeservlet.service.BalanceService;
import uz.jasur.cafeservlet.service.MealService;
import uz.jasur.cafeservlet.service.impl.BalanceServiceImpl;
import uz.jasur.cafeservlet.service.impl.MealServiceImpl;

import java.util.List;

public class MakeOrderCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        User user = (User) request.getSession().getAttribute("auth");
        String payment = request.getParameter("payment");
        boolean isCash = payment.equals("cash");
        int amount = Integer.parseInt(request.getParameter("amount"));
        String meal = request.getParameter("meal");
        String orderDueDate = date + " " + time;
        MealService mealService = new MealServiceImpl();
        int index = meal.indexOf("price");
        Meal mealFound = mealService.findByName(meal.substring(0, index));
        double totalCost = Double.parseDouble(meal.substring(index + 6)) * amount;
        Order order = new Order(user, orderDueDate, OrderStatus.NEW, true, amount, mealFound, totalCost, PayStatus.UNPAID);
        if (!isCash) {
            BalanceService balanceService = new BalanceServiceImpl();
            Balance foundBalance = balanceService.findByUserId(user.getId());
            if (foundBalance.getCashBalance() >= totalCost) {
                foundBalance.setCashBalance(foundBalance.getCashBalance() - totalCost);
                foundBalance.setLoyaltyBalance(foundBalance.getLoyaltyBalance() + totalCost * 0.01);
                BalanceDaoImpl balanceDao = new BalanceDaoImpl();
                if (balanceDao.update(foundBalance) != null) {
                    order.setPayStatus(PayStatus.PAID);
                }
            } else {
                order = null;
                request.setAttribute("welcome_msg", "Cash in your balance is not enough. Fill your balance.");
                return "pages/makeorder.jsp";
            }
        }
        OrderDaoImpl orderDao = new OrderDaoImpl();
        if (!orderDao.insert(order)) {
            request.setAttribute("welcome_msg", "Your order is created successfully. Do you want more?");
        } else {
            request.setAttribute("welcome_msg", "Your order is failed. Try once more.");
        }
        return "pages/user.jsp";
    }
}
