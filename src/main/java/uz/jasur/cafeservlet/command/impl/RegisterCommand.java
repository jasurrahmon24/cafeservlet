package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.BalanceDaoImpl;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.entity.enums.UserRole;
import uz.jasur.cafeservlet.entity.enums.UserStatus;
import uz.jasur.cafeservlet.service.UserService;
import uz.jasur.cafeservlet.service.impl.UserServiceImpl;

public class RegisterCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String login = request.getParameter("regLogin");
        String fullName = request.getParameter("fullName");
        String password = request.getParameter("password");
        String phoneNumber = request.getParameter("phoneNumber");
        String email = request.getParameter("email");
        UserService userService = UserServiceImpl.getInstance();
        User user = new User(login, fullName, password, email, phoneNumber, UserRole.USER, UserStatus.ACTIVE);
        String page;
        if (userService.register(user)) {
            User foundUser = userService.authenticate(login, password);
            Balance balance = new Balance(foundUser, 0.0, 0.0);
            BalanceDaoImpl balanceDao = new BalanceDaoImpl();
            if (!balanceDao.insert(balance)) {
                request.setAttribute("user", foundUser);
                request.setAttribute("login_msg", "Registration is successful. Please sign in.");
                page = "pages/login.jsp";
            } else {
                request.setAttribute("login_msg", "Balance is not created. Try once more");
                page = "pages/register.jsp";
            }
        } else {
            request.setAttribute("login_msg", "Registration failed. Try once more");
            page = "pages/register.jsp";
        }

        return page;
    }
}
