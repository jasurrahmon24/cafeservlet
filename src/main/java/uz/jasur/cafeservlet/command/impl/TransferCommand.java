package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.BalanceDaoImpl;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.service.BalanceService;
import uz.jasur.cafeservlet.service.impl.BalanceServiceImpl;

public class TransferCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String transfer = request.getParameter("transfer");
        int amount = Integer.parseInt(request.getParameter("amount"));
        BalanceService balanceService = new BalanceServiceImpl();
        User auth = (User) request.getSession().getAttribute("auth");
        Balance balance = balanceService.findByUserId(auth.getId());
        if (transfer.equals("withdrawal")) {
            if (balance.getCashBalance() >= amount) {
                balance.setCashBalance(balance.getCashBalance() - amount);
                request.setAttribute("transfer_msg", "Withdrawal happened");
            } else {
                request.setAttribute("transfer_msg", "Withdrawal did not happen");
            }
        } else {
            balance.setCashBalance(balance.getCashBalance() + amount);
                request.setAttribute("transfer_msg", "Deposit happened");
        }
        BalanceDaoImpl balanceDao = new BalanceDaoImpl();
        balanceDao.update(balance);
        return "/pages/user.jsp";
    }
}
