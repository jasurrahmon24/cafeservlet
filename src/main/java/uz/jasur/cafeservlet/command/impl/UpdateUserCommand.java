package uz.jasur.cafeservlet.command.impl;

import jakarta.servlet.http.HttpServletRequest;
import uz.jasur.cafeservlet.command.Command;
import uz.jasur.cafeservlet.dao.impl.UserDaoImpl;
import uz.jasur.cafeservlet.entity.User;

public class UpdateUserCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        String login = request.getParameter("regLogin");
        String fullName = request.getParameter("fullName");
        String password = request.getParameter("password");
        String phoneNumber = request.getParameter("phoneNumber");
        String email = request.getParameter("email");
        User auth = (User) request.getSession().getAttribute("auth");
        User updatedUser = new User(login, fullName, password, email, phoneNumber, auth.getUserRole(), auth.getUserStatus());
        updatedUser.setId(auth.getId());
        UserDaoImpl userDao = new UserDaoImpl();
        if(userDao.update(updatedUser) != null) {
            request.setAttribute("update_msg", "User updated successfully");
            request.getSession().setAttribute("auth", updatedUser);
        } else {
            request.setAttribute("update_msg", "User is not updated");
        }
        System.out.println(auth.getUserRole().getRole());
        if(auth.getUserRole().getRole().equals("admin")){
            return "pages/admin.jsp";
        } else {
            return "pages/user.jsp";
        }
    }
}
