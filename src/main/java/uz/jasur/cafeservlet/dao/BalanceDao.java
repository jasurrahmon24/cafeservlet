package uz.jasur.cafeservlet.dao;

import uz.jasur.cafeservlet.entity.Balance;

public interface BalanceDao{
    Balance findByUserId(int id);
}
