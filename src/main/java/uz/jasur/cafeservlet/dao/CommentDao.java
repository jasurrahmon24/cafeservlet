package uz.jasur.cafeservlet.dao;

import uz.jasur.cafeservlet.entity.Comment;

import java.util.List;

public interface CommentDao {

    List<Comment> findByMealId(int id);
}
