package uz.jasur.cafeservlet.dao;

import uz.jasur.cafeservlet.entity.Meal;

import java.util.List;
import java.util.Optional;

public interface MealDao {
    Meal findByName(String meal);

    List<Meal> findByRate(int rate);

    List<Meal> getPageable(int limit, int offset);

    Meal findById(int id);
}
