package uz.jasur.cafeservlet.dao;

import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.dto.OrderMealDto;

import java.util.HashMap;
import java.util.List;

public interface OrderDao {
    List<OrderMealDto> findOrdersMealsByUserId(int id);

    Order findById(int id);

    List<OrderMealDto> findAllOrderMeals();
}
