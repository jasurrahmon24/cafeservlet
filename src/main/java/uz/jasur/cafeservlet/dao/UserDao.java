package uz.jasur.cafeservlet.dao;

import uz.jasur.cafeservlet.entity.User;

public interface UserDao {
    User authenticate(String login, String password);
    boolean register(User user);

    User findById(int id);

    boolean updateStatus(User user);
}
