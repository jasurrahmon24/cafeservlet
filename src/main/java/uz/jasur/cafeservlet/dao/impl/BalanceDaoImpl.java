package uz.jasur.cafeservlet.dao.impl;

import uz.jasur.cafeservlet.dao.BalanceDao;
import uz.jasur.cafeservlet.dao.BaseDao;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.pool.ConnectionPool;
import uz.jasur.cafeservlet.service.UserService;
import uz.jasur.cafeservlet.service.impl.UserServiceImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class BalanceDaoImpl extends BaseDao<Balance> implements BalanceDao {
    @Override
    public boolean insert(Balance balance) {
        try {
            String query = "insert into balances(user_id, loyalty_balance, cash_balance) values(?,?,?)";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, balance.getUser().getId());
            ps.setDouble(2, balance.getCashBalance());
            ps.setDouble(3, balance.getLoyaltyBalance());
            ConnectionPool.getInstance().releaseConnection(con);
            return ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Balance balance) {
        return false;
    }

    @Override
    public List<Balance> findAll() {
        return null;
    }

    @Override
    public Balance update(Balance balance) {
        try{
            String query = "update balances set cash_balance = ?, loyalty_balance = ?, user_id = ? where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setDouble(1, balance.getCashBalance());
            ps.setDouble(2, balance.getLoyaltyBalance());
            ps.setInt(3, balance.getUser().getId());
            ps.setInt(4, balance.getId());
            ps.executeUpdate();
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return balance;
    }

    @Override
    public Balance findByUserId(int id) {
        Balance balance = new Balance();
        try {
            String query = "select * from balances where user_id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            UserService userService = UserServiceImpl.getInstance();
            if (rs.next()) {
                balance.setId(rs.getInt("id"));
                User user = userService.findById(id);
                user.setId(id);
                balance.setUser(user);
                balance.setCashBalance(rs.getDouble("cash_balance"));
                balance.setLoyaltyBalance(rs.getDouble("loyalty_balance"));
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return balance;
    }
}
