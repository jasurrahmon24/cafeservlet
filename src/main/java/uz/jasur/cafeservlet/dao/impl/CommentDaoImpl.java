package uz.jasur.cafeservlet.dao.impl;

import uz.jasur.cafeservlet.dao.BaseDao;
import uz.jasur.cafeservlet.dao.CommentDao;
import uz.jasur.cafeservlet.entity.Comment;
import uz.jasur.cafeservlet.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDaoImpl extends BaseDao<Comment> implements CommentDao {
    @Override
    public boolean insert(Comment comment) {
        Comment dto = null;
        try {
            String query = "insert into comments(text, order_id, meal_id, rate) values(?,?,?,?)";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, comment.getCommentText());
            ps.setInt(2, comment.getOrderId());
            ps.setInt(3, comment.getMealId());
            ps.setInt(4, comment.getCommentRate());
            ConnectionPool.getInstance().releaseConnection(con);
            return !ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Comment comment) {
        return false;
    }

    @Override
    public List<Comment> findAll() {
        return null;
    }

    @Override
    public Comment update(Comment comment) {
        return null;
    }

    @Override
    public List<Comment> findByMealId(int id) {
        List<Comment> comments = new ArrayList<>();
        try {
            String query = "select * from comments where meal_id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Comment comment = new Comment();
                comment.setCommentRate(rs.getInt("rate"));
                comment.setMealId(rs.getInt("meal_id"));
                comment.setOrderId(rs.getInt("order_id"));
                comment.setCommentText(rs.getString("text"));
                comment.setId(rs.getInt("id"));
                comments.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }
}
