package uz.jasur.cafeservlet.dao.impl;

import uz.jasur.cafeservlet.dao.BaseDao;
import uz.jasur.cafeservlet.dao.MealDao;
import uz.jasur.cafeservlet.entity.Meal;
import uz.jasur.cafeservlet.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MealDaoImpl extends BaseDao<Meal> implements MealDao {
    @Override
    public boolean insert(Meal meal) {
        try {
            String query = "insert into meals(name, price, description, order_rate, ingredients, image_src) values(?,?,?,?,?,?)";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, meal.getName());
            ps.setDouble(2, meal.getPrice());
            ps.setString(3, meal.getDescription());
            ps.setDouble(4, meal.getOrderRate());
            ps.setString(5, meal.getIngredients());
            ps.setString(6, meal.getFilePath());
            ConnectionPool.getInstance().releaseConnection(con);
            return ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Meal meal) {
        return false;
    }

    @Override
    public List<Meal> findAll() {
        List<Meal> meals = new ArrayList<>();
        try {
            String query = "SELECT * FROM meals";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Meal meal = new Meal();
                meal.setId(resultSet.getInt("id"));
                meal.setName(resultSet.getString("name"));
                meal.setPrice(resultSet.getDouble("price"));
                meal.setDescription(resultSet.getString("description"));
                meal.setOrderRate(resultSet.getDouble("order_rate"));
                meal.setIngredients(resultSet.getString("ingredients"));
                meal.setFilePath(resultSet.getString("image_src"));
                meals.add(meal);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return meals;
    }

    @Override
    public Meal update(Meal meal) {
        try {
            String query = "update meals set name = ?, price = ?, ingredients = ?, description = ? where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, meal.getName());
            ps.setDouble(2, meal.getPrice());
            ps.setString(3, meal.getIngredients());
            ps.setString(4, meal.getDescription());
            ps.setInt(5, meal.getId());
            if(ps.executeUpdate() != 0){
                ps.close();
                ConnectionPool.getInstance().releaseConnection(con);
                return meal;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Meal findByName(String mealName) {
        Meal meal = null;
        try {
            String query = "select * from meals where name = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, mealName);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                meal = new Meal();
                meal.setId(resultSet.getInt("id"));
                meal.setName(resultSet.getString("name"));
                meal.setPrice(resultSet.getDouble("price"));
                meal.setDescription(resultSet.getString("description"));
                meal.setOrderRate(resultSet.getDouble("order_rate"));
                meal.setIngredients(resultSet.getString("ingredients"));
                meal.setFilePath(resultSet.getString("image_src"));
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return meal;
    }

    @Override
    public List<Meal> findByRate(int rate) {
        List<Meal> meals = new ArrayList<>();
        try {
            String query = "SELECT * FROM meals where order_rate = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, rate);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Meal meal = new Meal();
                meal.setId(resultSet.getInt("id"));
                meal.setName(resultSet.getString("name"));
                meal.setPrice(resultSet.getDouble("price"));
                meal.setDescription(resultSet.getString("description"));
                meal.setOrderRate(resultSet.getDouble("order_rate"));
                meal.setIngredients(resultSet.getString("ingredients"));
                meal.setFilePath(resultSet.getString("image_src"));
                meals.add(meal);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return meals;
    }

    @Override
    public List<Meal> getPageable(int limit, int offset) {
        List<Meal> mealPages = new ArrayList<>();
        try {
            String query = "select * from meals order by id limit ? offset ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Meal meal = new Meal();
                meal.setName(rs.getString("name"));
                meal.setDescription(rs.getString("description"));
                meal.setPrice(rs.getDouble("price"));
                meal.setIngredients(rs.getString("ingredients"));
                meal.setOrderRate(rs.getInt("order_rate"));
                meal.setFilePath(rs.getString("image_src"));
                meal.setId(rs.getInt("id"));
                mealPages.add(meal);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mealPages;
    }

    @Override
    public Meal findById(int id) {
        Meal meal = null;
        try {
            String query = "select * from meals where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                meal = new Meal();
                meal.setId(resultSet.getInt("id"));
                meal.setName(resultSet.getString("name"));
                meal.setPrice(resultSet.getDouble("price"));
                meal.setDescription(resultSet.getString("description"));
                meal.setOrderRate(resultSet.getDouble("order_rate"));
                meal.setIngredients(resultSet.getString("ingredients"));
                meal.setFilePath(resultSet.getString("image_src"));
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return meal;
    }

}
