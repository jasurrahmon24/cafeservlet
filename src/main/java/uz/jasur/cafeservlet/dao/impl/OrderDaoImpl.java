package uz.jasur.cafeservlet.dao.impl;

import uz.jasur.cafeservlet.dao.BaseDao;
import uz.jasur.cafeservlet.dao.OrderDao;
import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.dto.OrderMealDto;
import uz.jasur.cafeservlet.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderDaoImpl extends BaseDao<Order> implements OrderDao {
    @Override
    public boolean insert(Order order) {
        try {
            String query = "insert into orders(user_id, order_due_date, status, cash, meal_id, quantity, total_cost, pay_status) values(?,?,?,?,?,?,?,?)";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, order.getUser().getId());
            ps.setString(2, order.getOrderDueDate());
            ps.setString(3, order.getStatus().getStatus());
            ps.setBoolean(4, order.isCash());
            ps.setInt(5, order.getMeal().getId());
            ps.setInt(6, order.getAmount());
            ps.setDouble(7, order.getTotal());
            ps.setString(8, order.getPayStatus().getStatus());
            ConnectionPool.getInstance().releaseConnection(con);
            return ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean delete(Order order) {
        return false;
    }

    @Override
    public List<Order> findAll() {
        return null;
    }

    @Override
    public Order update(Order order) {
        try {
            String query = "update orders set status = ?, pay_status = ? where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, order.getStatus().getStatus());
            ps.setString(2, order.getPayStatus().getStatus());
            ps.setInt(3, order.getId());
            if(ps.executeUpdate() != 0){
                ps.close();
                ConnectionPool.getInstance().releaseConnection(con);
                return order;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<OrderMealDto> findOrdersMealsByUserId(int id) {
        List<OrderMealDto> list = new ArrayList<>();
        try {
            String query = "select users.full_name, orders.id, orders.status, orders.meal_id, orders.total_cost, orders.pay_status, orders.order_due_date, orders.quantity, meals.name, meals.image_src from orders join meals on orders.meal_id = meals.id join users on users.id = orders.user_id where user_id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderMealDto dto = new OrderMealDto();
                dto.setOrderId(rs.getInt("id"));
                dto.setOrderStatus(rs.getString("status"));
                dto.setQuantity(rs.getInt("quantity"));
                dto.setMealName(rs.getString("name"));
                dto.setDueDate(rs.getString("order_due_date"));
                dto.setMealId(rs.getInt("meal_id"));
                dto.setTotalCost(rs.getDouble("total_cost"));
                dto.setPayStatus(rs.getString("pay_status"));
                dto.setImage(rs.getString("image_src"));
                list.add(dto);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Order findById(int id) {
        return null;
    }

    @Override
    public List<OrderMealDto> findAllOrderMeals() {
        List<OrderMealDto> list = new ArrayList<>();
        try {
            String query = "select users.full_name, orders.id, orders.status, orders.meal_id, orders.total_cost, orders.pay_status, orders.order_due_date, orders.quantity, meals.name, meals.image_src from orders join meals on orders.meal_id = meals.id join users on users.id = orders.user_id";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                OrderMealDto dto = new OrderMealDto();
                dto.setOrderId(rs.getInt("id"));
                dto.setOrderStatus(rs.getString("status"));
                dto.setMealId(rs.getInt("meal_id"));
                dto.setTotalCost(rs.getDouble("total_cost"));
                dto.setPayStatus(rs.getString("pay_status"));
                dto.setQuantity(rs.getInt("quantity"));
                dto.setMealName(rs.getString("name"));
                dto.setDueDate(rs.getString("order_due_date"));
                dto.setImage(rs.getString("image_src"));
                dto.setFullName(rs.getString("full_name"));
                list.add(dto);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
