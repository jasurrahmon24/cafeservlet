package uz.jasur.cafeservlet.dao.impl;

import uz.jasur.cafeservlet.dao.BaseDao;
import uz.jasur.cafeservlet.dao.UserDao;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.entity.enums.UserRole;
import uz.jasur.cafeservlet.entity.enums.UserStatus;
import uz.jasur.cafeservlet.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends BaseDao<User> implements UserDao {
    private static final UserDaoImpl instance = new UserDaoImpl();

    public static UserDaoImpl getInstance() {
        return instance;
    }

    @Override
    public boolean insert(User user) {
        return false;
    }

    @Override
    public boolean delete(User user) {
        return false;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            String query = "select * from users";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setUserRole(UserRole.valueOf(resultSet.getString("role").toUpperCase()));
                user.setUserStatus(UserStatus.valueOf(resultSet.getString("status").toUpperCase()));
                user.setFullName(resultSet.getString("full_name"));
                users.add(user);
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User update(User user) {
        try {
            String query = "update users set login = ?, full_name = ?, password = ?, email = ?, phone_number = ? where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getFullName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getEmail());
            ps.setString(5, user.getPhoneNumber());
            ps.setInt(6, user.getId());
            if (ps.executeUpdate() != 0) {
                ps.close();
                ConnectionPool.getInstance().releaseConnection(con);
                return user;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    @Override
    public User authenticate(String login, String password) {
        User user = null;
        try {
            String query = "SELECT * FROM users where login = ? and password = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setUserRole(UserRole.valueOf(resultSet.getString("role").toUpperCase()));
                user.setUserStatus(UserStatus.valueOf(resultSet.getString("status").toUpperCase()));
                user.setFullName(resultSet.getString("full_name"));
                user.setPhoneNumber(resultSet.getString("phone_number"));
                user.setEmail(resultSet.getString("email"));
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean register(User user) {
        try {
            String query = "insert into users(login, full_name, password, email, role, status, phone_number) values (?,?,?,?,?,?,?)";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getFullName());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getEmail());
            ps.setString(5, user.getUserRole().getRole());
            ps.setString(6, user.getUserStatus().getStatus());
            ps.setString(7, user.getPhoneNumber());
            ps.execute();
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public User findById(int id) {
        try {
            String query = "select * from users where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                user.setFullName(rs.getString("full_name"));
                user.setEmail(rs.getString("email"));
                user.setPhoneNumber(rs.getString("phone_number"));
                user.setUserRole(UserRole.valueOf(rs.getString("role").toUpperCase()));
                user.setUserStatus(UserStatus.valueOf(rs.getString("status").toUpperCase()));
                user.setPassword(rs.getString("password"));
                ps.close();
                ConnectionPool.getInstance().releaseConnection(con);
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateStatus(User user) {
        try {
            String query = "update users set status = ? where id = ?";
            Connection con = ConnectionPool.getInstance().getConnection();
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, user.getUserStatus().getStatus());
            ps.setInt(2, user.getId());
            if (ps.executeUpdate() != 0) {
                return true;
            }
            ps.close();
            ConnectionPool.getInstance().releaseConnection(con);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
