package uz.jasur.cafeservlet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Balance extends AbstractEntity {
    private User user;
    private double loyaltyBalance;
    private double cashBalance;
}
