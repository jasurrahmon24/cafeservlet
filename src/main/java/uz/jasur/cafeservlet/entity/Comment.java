package uz.jasur.cafeservlet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.jasur.cafeservlet.entity.AbstractEntity;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Comment extends AbstractEntity {
private int mealId;
private int orderId;
private String commentText;
private int commentRate;
}
