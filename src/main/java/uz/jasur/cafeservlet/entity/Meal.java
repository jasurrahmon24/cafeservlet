package uz.jasur.cafeservlet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Meal extends AbstractEntity{
    private String name;
    private double price;
    private String description;
    private double orderRate;
    private String ingredients;
    private String filePath;
}
