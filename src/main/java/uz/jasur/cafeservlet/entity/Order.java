package uz.jasur.cafeservlet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.jasur.cafeservlet.entity.enums.OrderStatus;
import uz.jasur.cafeservlet.entity.enums.PayStatus;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Order extends AbstractEntity {
    private User user;
    private String orderDueDate;
    private OrderStatus status;
    private boolean isCash;
    private int amount;
    private Meal meal;
    private double total;
    private PayStatus payStatus;

    public Order(int id, OrderStatus status, PayStatus payStatus) {
        super(id);
        this.status = status;
        this.payStatus = payStatus;
    }
}
