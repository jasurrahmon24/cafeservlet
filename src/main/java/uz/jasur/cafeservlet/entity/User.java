package uz.jasur.cafeservlet.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.jasur.cafeservlet.entity.enums.UserRole;
import uz.jasur.cafeservlet.entity.enums.UserStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User extends AbstractEntity{
    private String login;
    private String fullName;
    private String password;
    private String email;
    private String phoneNumber;
    private UserRole userRole;
    private UserStatus userStatus;
}
