package uz.jasur.cafeservlet.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderMealDto {
private int orderId;
private String orderStatus;
private int mealId;
private double totalCost;
private String payStatus;
private String dueDate;
private int quantity;
private String mealName;
private String image;
private String fullName;
}
