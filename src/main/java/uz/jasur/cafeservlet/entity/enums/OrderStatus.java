package uz.jasur.cafeservlet.entity.enums;

public enum OrderStatus {
    NEW,
    READY,
    SERVED;
    public String getStatus(){
        return this.toString().toLowerCase();
    }
}
