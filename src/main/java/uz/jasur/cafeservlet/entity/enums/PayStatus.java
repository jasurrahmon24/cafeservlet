package uz.jasur.cafeservlet.entity.enums;

public enum PayStatus {
    PAID,
    UNPAID;
    public String getStatus(){
        return this.toString().toLowerCase();
    }
}
