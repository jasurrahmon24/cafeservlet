package uz.jasur.cafeservlet.entity.enums;

public enum UserRole {
    ADMIN,
    USER;

    public String getRole(){
        return this.toString().toLowerCase();
    }
}
