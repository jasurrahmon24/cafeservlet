package uz.jasur.cafeservlet.entity.enums;

public enum UserStatus {
    ACTIVE,
    BLOCKED;
    public String getStatus(){
        return this.toString().toLowerCase();
    }
}
