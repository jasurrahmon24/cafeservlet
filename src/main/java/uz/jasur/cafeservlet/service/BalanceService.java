package uz.jasur.cafeservlet.service;

import uz.jasur.cafeservlet.entity.Balance;

public interface BalanceService {
    Balance findByUserId(int id);
}
