package uz.jasur.cafeservlet.service;

import uz.jasur.cafeservlet.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> findByMealId(int id);
}
