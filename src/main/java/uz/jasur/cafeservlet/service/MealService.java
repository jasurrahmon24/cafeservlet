package uz.jasur.cafeservlet.service;

import uz.jasur.cafeservlet.entity.Meal;

import java.util.List;

public interface MealService {
    Meal findByName(String meal);
    Meal findById(int id);
    List<Meal> findByRate(int rate);
    List<Meal> getPageable(int limit, int offset);
}
