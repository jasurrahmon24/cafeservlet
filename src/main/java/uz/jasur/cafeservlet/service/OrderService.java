package uz.jasur.cafeservlet.service;

import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.dto.OrderMealDto;

import java.util.HashMap;
import java.util.List;

public interface OrderService {
    List<OrderMealDto> findOrdersMealsByUserId(int id);
    List<OrderMealDto> findAllOrdersMeals();
    Order findById(int id);
}
