package uz.jasur.cafeservlet.service;

import uz.jasur.cafeservlet.entity.User;

public interface UserService {
    boolean register(User user);
    User authenticate(String login, String password);
boolean updateStatus(User user);
    User findById(int id);
}
