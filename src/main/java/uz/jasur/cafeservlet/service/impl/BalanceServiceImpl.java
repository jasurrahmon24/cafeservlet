package uz.jasur.cafeservlet.service.impl;

import uz.jasur.cafeservlet.dao.BalanceDao;
import uz.jasur.cafeservlet.dao.impl.BalanceDaoImpl;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.service.BalanceService;
import uz.jasur.cafeservlet.service.UserService;

public class BalanceServiceImpl implements BalanceService {
    @Override
    public Balance findByUserId(int id) {
        BalanceDao balanceDao = new BalanceDaoImpl();
        return balanceDao.findByUserId(id);
    }
}
