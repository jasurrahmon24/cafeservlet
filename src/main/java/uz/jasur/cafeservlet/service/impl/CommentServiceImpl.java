package uz.jasur.cafeservlet.service.impl;

import uz.jasur.cafeservlet.dao.CommentDao;
import uz.jasur.cafeservlet.dao.impl.CommentDaoImpl;
import uz.jasur.cafeservlet.entity.Comment;
import uz.jasur.cafeservlet.service.CommentService;

import java.util.List;

public class CommentServiceImpl implements CommentService {
    @Override
    public List<Comment> findByMealId(int id) {
        CommentDao commentDao = new CommentDaoImpl();
        return commentDao.findByMealId(id);
    }
}
