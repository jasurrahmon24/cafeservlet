package uz.jasur.cafeservlet.service.impl;

import uz.jasur.cafeservlet.dao.impl.MealDaoImpl;
import uz.jasur.cafeservlet.entity.Meal;
import uz.jasur.cafeservlet.service.MealService;

import java.util.List;

public class MealServiceImpl implements MealService {
    MealDaoImpl mealDao = new MealDaoImpl();

    @Override
    public Meal findByName(String meal) {
        return mealDao.findByName(meal);
    }

    @Override
    public Meal findById(int id) {
        return mealDao.findById(id);
    }

    @Override
    public List<Meal> findByRate(int rate) {
        return mealDao.findByRate(rate);
    }

    @Override
    public List<Meal> getPageable(int limit, int offset) {
        return mealDao.getPageable(limit, offset);
    }
}
