package uz.jasur.cafeservlet.service.impl;

import uz.jasur.cafeservlet.dao.OrderDao;
import uz.jasur.cafeservlet.dao.impl.OrderDaoImpl;
import uz.jasur.cafeservlet.entity.Order;
import uz.jasur.cafeservlet.entity.dto.OrderMealDto;
import uz.jasur.cafeservlet.service.OrderService;

import java.util.HashMap;
import java.util.List;

public class OrderServiceImpl implements OrderService {
        OrderDao orderDao = new OrderDaoImpl();
    @Override
    public List<OrderMealDto> findOrdersMealsByUserId(int id) {
        return orderDao.findOrdersMealsByUserId(id);
    }

    @Override
    public List<OrderMealDto> findAllOrdersMeals() {
        return orderDao.findAllOrderMeals();
    }

    @Override
    public Order findById(int id) {
        return orderDao.findById(id);
    }
}
