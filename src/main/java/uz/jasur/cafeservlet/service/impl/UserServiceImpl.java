package uz.jasur.cafeservlet.service.impl;

import uz.jasur.cafeservlet.dao.impl.UserDaoImpl;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.service.UserService;

public class UserServiceImpl implements UserService {
    private static final UserServiceImpl instance = new UserServiceImpl();

    private UserServiceImpl() {
    }

    public static UserServiceImpl getInstance() {
        return instance;
    }
    @Override
    public boolean register(User user) {
        UserDaoImpl userDao = UserDaoImpl.getInstance();
        return userDao.register(user);
    }

    @Override
    public User authenticate(String login, String password) {
        UserDaoImpl userDao = UserDaoImpl.getInstance();
        return userDao.authenticate(login, password);
    }

    @Override
    public boolean updateStatus(User user) {
        UserDaoImpl userDao = UserDaoImpl.getInstance();
        return userDao.updateStatus(user);
    }

    @Override
    public User findById(int id) {
        UserDaoImpl userDao = UserDaoImpl.getInstance();
        return userDao.findById(id);
    }
}
