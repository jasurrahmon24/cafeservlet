<%@ page import="uz.jasur.cafeservlet.service.MealService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.MealServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Meal" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Cafe Project"/>
</jsp:include>

<main class="container">
    <h1 class="text-center">Welcome to Our Cafe!</h1>
    <h3 class="text-center">Choose one of our Recommended meals and Enjoy it.</h3>
    <%
        MealService mealService = new MealServiceImpl();
        List<Meal> recMeals = mealService.findByRate(5);
        request.setAttribute("recMeals", recMeals);
    %>
    <div class="card-wrapper">
        <c:forEach items="${recMeals}" var="meal">
            <a href="${pageContext.request.contextPath}/pages/mealPage.jsp?id=${meal.getId()}" class="card card-style col-lg-3 m-4" >
                <div class="img">
                    <img src="${pageContext.request.contextPath}${meal.getFilePath()}" class="card-img-top"
                         alt="${meal.getName()}">
                </div>
                <div class="card-body">
                    <h5 class="card-title">${meal.getName()} </h5>
                    <p class="card-text">Description: ${meal.getDescription()} </p>
                    <p class="card-text">Ingredients: ${meal.getIngredients()} </p>
                    <button class="btn btn-outline-primary">Comments</button>
                </div>
            </a>
        </c:forEach>
    </div>
</main>


<jsp:include page="/pages/components/footer.jsp"/>