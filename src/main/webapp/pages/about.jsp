<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="About Page"/>
</jsp:include>

<main class="container">
    <div class="card-wrapper">
        <div class="card text-center col-lg-5 m-4">
            <div class="card-header">
                Created for
            </div>
            <div class="card-body">
                <img class="card-img-top rounded-circle w-25 mx-auto" src="https://tdb.ua/upload/customer/2020/04/EPAM_Systems_color.png" alt="EPAM logo">
                <h5 class="card-title">EPAM Systems</h5>
                <p class="card-text">In order to be qualified for EPAM Lab</p>
                <a target="_blank" href="https://www.epam.com/" class="btn btn-primary">Official website</a>
            </div>
            <div class="card-footer text-muted">
                2 days ago
            </div>
        </div>
        <div class="card text-center col-lg-5 m-4">
            <div class="card-header">
                Created By
            </div>
            <div class="card-body">
                <img class="card-img-top rounded-circle w-25 mx-auto" src="${pageContext.request.contextPath}/img/creator.png" alt="owner">
                <h5 class="card-title mt-3">Jasur Rahmonov</h5>
                <p class="card-text">Junior Java developer learning eagerly</p>
                <a target="_blank" href="https://jasurrahmonov.netlify.app/" class="btn btn-primary">Portfolio website</a>
            </div>
            <div class="card-footer text-muted">
                2 days ago
            </div>
        </div>
        <div class="card text-center col-lg-5 m-4">
            <div class="card-header">
                Saved at
            </div>
            <div class="card-body">
                <img class="card-img-top rounded-circle w-25 mx-auto" src="https://matrix.org/docs/projects/images/gitlab.png" alt="gitlab">
                <h5 class="card-title">Source code</h5>
                <p class="card-text">With this repo, you can see project codes</p>
                <a target="_blank" href="https://gitlab.com/jasurrahmon24/cafeservlet" class="btn btn-primary">Link to repo</a>
            </div>
            <div class="card-footer text-muted">
                2 days ago
            </div>
        </div>
    </div>
</main>

<jsp:include page="/pages/components/footer.jsp"/>