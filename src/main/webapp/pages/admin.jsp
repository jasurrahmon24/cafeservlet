<%@ page import="uz.jasur.cafeservlet.service.OrderService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.OrderServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.dto.OrderMealDto" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.jasur.cafeservlet.dao.impl.UserDaoImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.User" %>
<%@ page import="uz.jasur.cafeservlet.dao.impl.MealDaoImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Meal" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Admin Cabinet Page"/>
</jsp:include>
<c:if test="${auth == null}">
    <c:redirect url = "login.jsp"/>
</c:if>
<c:if test="${auth.getUserRole().getRole() eq 'user'}">
    <c:redirect url = "user.jsp"/>
</c:if>
<%
    OrderService orderService = new OrderServiceImpl();
    UserDaoImpl userDao = new UserDaoImpl();
    MealDaoImpl mealDao = new MealDaoImpl();
    List<OrderMealDto> orders = orderService.findAllOrdersMeals();
    List<User> allUsers = userDao.findAll();
    List<Meal> allMeals = mealDao.findAll();
    request.setAttribute("allOrders", orders);
    request.setAttribute("allUsers", allUsers);
    request.setAttribute("allMeals", allMeals);
%>
<main class="container">
    <div class="d-flex flex-grow-1 mt-4">
        <div class="nav flex-column col-lg-3 nav-pills pe-3" id="v-pills-tab" role="tablist"
             aria-orientation="vertical">
            <button class="nav-link w-100 mb-4 active" id="v-pills-home-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home"
                    aria-selected="true">Admin info
            </button>
            <button class="nav-link w-100 mb-4" id="v-pills-profile-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile"
                    aria-selected="false">Meals
            </button>
            <button class="nav-link w-100 mb-4" id="v-pills-messages-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages"
                    aria-selected="false">Orders
            </button>
            <button class="nav-link w-100 mb-4" id="v-pills-users-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-users" type="button" role="tab" aria-controls="v-pills-users"
                    aria-selected="false">Users
            </button>
        </div>

        <div class="tab-content col-lg-9" id="v-pills-tabContent">

            <div class="tab-pane fade show active orders" id="v-pills-home" role="tabpanel"
                 aria-labelledby="v-pills-home-tab" tabindex="0">
                <h3 class="text-center">${update_msg}</h3>
                <form class="form mt-0" action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="update_user"/>
                    <div class="mb-3">
                        <label class="form-label" for="regLogin">Change login: </label>
                        <input class="form-control" id="regLogin" required type="text" name="regLogin"
                               value="${auth.getLogin()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="fullName">Change FullName: </label>
                        <input class="form-control" id="fullName" required type="text" name="fullName"
                               value="${auth.getFullName()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="password">Change Password: </label>
                        <input class="form-control" id="password" required type="password" name="password"
                               value="${auth.getPassword()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="email">Change Email: </label>
                        <input class="form-control" id="email" required type="email" name="email"
                               value="${auth.getEmail()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="phoneNumber">Change PhoneNumber: </label>
                        <input class="form-control" id="phoneNumber" required type="text" name="phoneNumber"
                               value="${auth.getPhoneNumber()}"/>
                    </div>
                    <button type="submit" class="btn btn-outline-primary mt-2">Update</button>
                </form>
            </div>

            <div class="tab-pane fade profile" id="v-pills-profile" role="tabpanel"
                 aria-labelledby="v-pills-profile-tab" tabindex="0">
                <div class="d-flex justify-content-between mb-3">
                    <h3>Hello, ${auth.getFullName()}. You want to add some meal?</h3>
                    <button class="btn btn-primary p-2" id="addMeal" data-bs-toggle="modal" data-bs-target="#mealAddModal">+Add meal</button>
                </div>
                <c:forEach items="${allMeals}" var="meal">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h4 class="card-title">Meal name: ${meal.getName()}</h4>
                                    <div class="card-info d-flex flex-wrap mb-1">
                                        <div class="btn d-flex align-items-center btn-light mb-2 me-3">
                                            <h6 class="text-muted mb-0">Meal price: </h6>
                                            <p class="card-text px-2">${meal.getPrice()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light  align-items-center mb-2 me-3">
                                            <h6 class="text-muted mb-0">Order rate: </h6>
                                            <p class="card-text px-2">${meal.getOrderRate()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3  align-items-center">
                                            <h6 class="text-muted mb-0">Description: </h6>
                                            <p class="card-text px-2">${meal.getDescription()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3 align-items-center">
                                            <h6 class="text-muted mb-0">Ingredients: </h6>
                                            <p class="card-text px-2">${meal.getIngredients()}</p>
                                        </div>
                                    </div>
                                    <button data-bs-toggle="modal" id="mealEdit" data-bs-target="#mealModal"
                                            type="button" onclick="getMealId(this)"
                                            class="btn btn-outline-primary w-50"
                                            data-meal-id="${meal.getId()}"
                                            data-meal-name="${meal.getName()}"
                                            data-meal-price="${meal.getPrice()}"
                                            data-meal-description="${meal.getDescription()}"
                                            data-meal-ingredients="${meal.getIngredients()}"
                                    >
                                            Edit meal
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex justify-content-end">
                                <img src="${pageContext.request.contextPath}${meal.getFilePath()}"
                                     class="img-fluid rounded-end h-100" alt="${meal.getName()}">
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>

            <div class="tab-pane fade balance" id="v-pills-messages" role="tabpanel"
                 aria-labelledby="v-pills-messages-tab" tabindex="0">
                <h3 class="text-center mb-3">${order_status_msg}</h3>
                <c:forEach items="${allOrders}" var="order">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h4 class="card-title">${order.getFullName()}: ${order.getMealName()}</h4>
                                    <div class="card-info d-flex flex-wrap mb-1">
                                        <div class="btn d-flex align-items-center btn-light mb-2 me-3">
                                            <h6 class="text-muted mb-0">Due date: </h6>
                                            <p class="card-text px-2">${order.getDueDate()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light  align-items-center mb-2 me-3">
                                            <h6 class="text-muted mb-0">Total cost: </h6>
                                            <p class="card-text px-2">${order.getTotalCost()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3  align-items-center">
                                            <h6 class="text-muted mb-0">Quantity: </h6>
                                            <p class="card-text px-2">${order.getQuantity()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3 align-items-center">
                                            <h6 class="text-muted mb-0">Pay status: </h6>
                                            <p class="card-text px-2">${order.getPayStatus()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3 align-items-center">
                                            <h6 class="text-muted mb-0">Order status: </h6>
                                            <p class="card-text px-2">${order.getOrderStatus()}</p>
                                        </div>
                                    </div>
                                    <button data-bs-toggle="modal" id="orderRate" data-bs-target="#orderModal"
                                            type="button" onclick="getId(this)"
                                            class="btn btn-outline-primary w-50" data-order-id="${order.getOrderId()}">
                                        Change statuses
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex justify-content-end">
                                <img src="${pageContext.request.contextPath}${order.getImage()}"
                                     class="img-fluid rounded-end h-100" alt="${order.getMealName()}">
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>

            <div class="tab-pane fade balance" id="v-pills-users" role="tabpanel"
                 aria-labelledby="v-pills-users-tab" tabindex="0">
                <h3 class="text-center mb-3">${user_status_msg}</h3>
                <table class="table table-striped p-2 border-1 rounded">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Full name</th>
                        <th scope="col">Login</th>
                        <th scope="col">Role</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:set var="count" value="0" scope="page"/>
                    <c:forEach items="${allUsers}" var="user">
                        <c:set var="count" value="${count + 1}" scope="page"/>
                        <tr>
                            <th scope="row">${count}</th>
                            <td>${user.getFullName()}</td>
                            <td>${user.getLogin()}</td>
                            <td>${user.getUserRole()}</td>
                            <td>
                                <div class="form-check form-switch d-flex justify-content-start">
                                    <input class="form-check-input" name="userStatus" type="checkbox"
                                           id="flexSwitchCheckDefault" data-bs-toggle="modal" onchange="getThis(this)"
                                           data-bs-target="#userModal" data-userId-from-users="${user.getId()}"
                                    <c:if test="${user.getUserStatus().getStatus() eq 'active'}">
                                           checked
                                    </c:if>
                                    >
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</main>
<div class="modal fade" id="orderModal" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Rate order</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <form action="${pageContext.request.contextPath}/controller">
                <input type="hidden" name="command" value="change_statuses"/>
                <input type="hidden" id="orderId" name="orderId" value=""/>
                <div class="modal-body mx-2">
                    <div class="row mb-3">
                        <select class="form-select" aria-label="Default select example" name="payStatus" required>
                            <option selected value="unpaid">Unpaid</option>
                            <option value="paid">Paid</option>
                        </select>
                    </div>
                    <div class="row mb-3">
                        <select class="form-select" aria-label="Default select example" name="orderStatus" required>
                            <option selected value="new">New</option>
                            <option value="ready">Ready</option>
                            <option value="served">Served</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="mealModal" tabindex="-1" aria-labelledby="mealModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="mealModalLabel">Edit meal</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <form action="${pageContext.request.contextPath}/controller">
                <input type="hidden" name="command" value="edit_meal"/>
                <input type="hidden" id="mealId" name="mealId" value=""/>
                <div class="modal-body mx-2">
                    <div class="row mb-3">
                        <input id="mealName" type="text" name="mealName" class="form-control" placeholder="Enter name of meal" required/>
                    </div>
                    <div class="row mb-3">
                        <input id="mealPrice" type="number" name="mealPrice" class="form-control" placeholder="Enter price of meal" required/>
                    </div>
                    <div class="row mb-3">
                        <textarea id="mealIngredient" class="form-control" name="mealIngredient" required placeholder="Enter ingredients of meal"></textarea>
                    </div>
                    <div class="row mb-3">
                        <textarea class="form-control" id="mealDescription" name="mealDescription" required placeholder="Enter description of meal"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">Edit meal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="mealAddModal" tabindex="-1" aria-labelledby="mealAddModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="mealAddModalLabel">Add meal</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <form action="${pageContext.request.contextPath}/controller" enctype="multipart/form-data" method="post">
                <input type="hidden" name="command" value="add_meal"/>
                <div class="modal-body mx-2">
                    <div class="row mb-3">
                        <input type="text" name="mealName" class="form-control" placeholder="Enter name of meal" required/>
                    </div>
                    <div class="row mb-3">
                        <input type="number" name="mealPrice" class="form-control" placeholder="Enter price of meal" required/>
                    </div>
                    <div class="row mb-3">
                        <textarea class="form-control" name="mealIngredient" required placeholder="Enter ingredients of meal"></textarea>
                    </div>
                    <div class="row mb-3">
                        <select class="form-select" aria-label="Default select example" name="mealRate" required>
                            <option selected value="5">5</option>
                            <option value="4">4</option>
                            <option value="3">3</option>
                            <option value="2">2</option>
                            <option value="1">1</option>
                        </select>
                    </div>
                    <div class="row mb-3">
                        <textarea class="form-control" name="mealDescription" required placeholder="Enter description of meal"></textarea>
                    </div>
                    <div class="row mb-3">
                    <input class="form-control" type="file" name="imageSrc" required placeholder="Choose image of meal"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel
                    </button>
                    <button type="submit" class="btn btn-primary">Add new meal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="userModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5">User status change</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Do you really want to change user status ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                <form action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="change_user_status"/>
                    <input type="hidden" id="userId" name="userIdFromUsers" value=""/>
                    <input type="hidden" id="userStatus" name="userStatusFromUsers" value=""/>
                    <button type="submit" class="btn btn-primary">Yes, I do</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function getThis(id) {
        document.getElementById("userId").value = id.getAttribute("data-userId-from-users");
        if (id.checked) {
            document.getElementById("userStatus").value = "ACTIVE";
        } else {
            document.getElementById("userStatus").value = "BLOCKED";
        }
    }

    function getId(id) {
        document.getElementById("orderId").value = id.getAttribute("data-order-id");
    }

    function getMealId(id) {
        document.getElementById("mealId").value = id.getAttribute("data-meal-id");
        document.getElementById("mealName").value = id.getAttribute("data-meal-name");
        document.getElementById("mealPrice").value = id.getAttribute("data-meal-price");
        document.getElementById("mealIngredient").value = id.getAttribute("data-meal-ingredients");
        document.getElementById("mealDescription").value = id.getAttribute("data-meal-description");
    }

</script>
<jsp:include page="/pages/components/footer.jsp"/>