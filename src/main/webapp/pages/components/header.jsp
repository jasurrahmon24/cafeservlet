<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11/21/2022
  Time: 4:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${param.title}</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<header class="container fixed-top">
    <a href="${pageContext.request.contextPath}/" class="logo">
        <img src="${pageContext.request.contextPath}/img/logo.png" alt="logo_png"/>
    </a>
    <ul class="nav justify-content-center">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page"
               href="${pageContext.request.contextPath}/pages/cuisine.jsp?page=1">Cuisine</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/pages/makeorder.jsp">Make Order</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/pages/about.jsp">About Project</a>
        </li>
        <c:if test="${auth != null}">
            <li class="nav-item">
                <c:if test="${auth.getUserRole().getRole() eq 'admin'}">
                    <a class="nav-link" href="${pageContext.request.contextPath}/pages/admin.jsp">Admin Cabinet</a>
                </c:if>
                <c:if test="${auth.getUserRole().getRole() eq 'user'}">
                    <a class="nav-link" href="${pageContext.request.contextPath}/pages/user.jsp">My Cabinet</a>
                </c:if>
            </li>
        </c:if>
    </ul>
    <c:set var="signedUser" scope="session" value="${auth}"/>
    <c:if test="${signedUser == null}">
        <button type="button" class="btn btn-outline-primary m-0">
            <a class="nav-link" href="${pageContext.request.contextPath}/pages/login.jsp">Login</a>
        </button>
    </c:if>
    <c:if test="${signedUser != null}">
        <form class="m-0" action="${pageContext.request.contextPath}/controller">
            <input type="hidden" name="command" value="logout">
            <button class="btn btn-outline-primary" type="submit">Logout</button>
        </form>
    </c:if>
    <%--    <c:out value="${user}"/>--%>
</header>



