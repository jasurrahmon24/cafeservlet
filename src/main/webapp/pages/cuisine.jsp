<%@ page import="uz.jasur.cafeservlet.service.MealService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.MealServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Meal" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.jasur.cafeservlet.dao.impl.MealDaoImpl" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Cuisine Page"/>
</jsp:include>

<main class="container">
    <h1 class="text-center">Our menu</h1>
    <%
        int limit = 6;
        int pageNum = Integer.parseInt(request.getParameter("page"));
        MealService mealService = new MealServiceImpl();
        MealDaoImpl mealDao = new MealDaoImpl();
        List<Meal> allMeals = mealDao.findAll();
        int numOfPages = (int) Math.ceil(allMeals.size() / limit);
        List<Meal> mealsByPage = mealService.getPageable(limit, limit * (pageNum - 1));
        request.setAttribute("numOfPages", numOfPages);
        request.setAttribute("mealsByPage", mealsByPage);
    %>
    <div class="card-wrapper">
        <c:forEach items="${mealsByPage}" var="meal">
            <a href="${pageContext.request.contextPath}/pages/mealPage.jsp?id=${meal.getId()}" class="card card-style col-lg-3 m-4">
                <div class="img">
                    <img src="${pageContext.request.contextPath}${meal.getFilePath()}" class="card-img-top"
                         alt="${meal.getName()}">
                </div>
                <div class="card-body">
                    <h5 class="card-title">${meal.getName()} </h5>
                    <p class="card-text">Description: ${meal.getDescription()} </p>
                    <p class="card-text">Ingredients: ${meal.getIngredients()} </p>
                    <button class="btn btn-outline-primary">Comments</button>
                </div>
            </a>
        </c:forEach>
    </div>
    <c:if test="${param.page > 1}">
        <c:set var="pagePreviousLink"
               value="${pageContext.request.contextPath}/pages/cuisine.jsp?page=${param.page - 1}"/>
    </c:if>
    <c:if test="${param.page < numOfPages}">
        <c:set var="pageNextLink" value="${pageContext.request.contextPath}/pages/cuisine.jsp?page=${param.page + 1}"/>
    </c:if>
    <ul class="pagination d-flex justify-content-center">
        <li class="page-item">
            <a class="page-link" href="${pagePreviousLink}">Previous</a>
        </li>
        <c:forEach var="i" begin="1" end="${numOfPages + 1}">
            <li class="page-item">
                <a class="page-link" href="${pageContext.request.contextPath}/pages/cuisine.jsp?page=${i}">${i}</a>
            </li>
        </c:forEach>
        <li class="page-item">
            <a class="page-link" href="${pageNextLink}">Next</a>
        </li>
    </ul>
</main>

<jsp:include page="/pages/components/footer.jsp"/>
