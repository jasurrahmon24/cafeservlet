<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Login Page"/>
</jsp:include>
<main class="container">
    <h3 class="text-center">${login_msg}</h3>
    <form class="form" action="${pageContext.request.contextPath}/controller" autocomplete="on">
        <input type="hidden" name="command" value="login"/>
        <div class="mb-3">
            <label for="login" class="form-label">Login</label>
            <input name="login" type="text" class="form-control" id="login" required value="<c:if test="${user != null}">${user.getLogin()}</c:if>"/>
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" name="password" class="form-control" id="password" required value="<c:if test="${user != null}">${user.getPassword()}</c:if>"/>
        </div>
        <div class="btn-group mt-2 align-content-center">
            <button type="submit" class="btn btn-outline-primary">Sign In</button>
            <button type="button" class="btn btn-outline-primary">
                <a class="nav-link" href="${pageContext.request.contextPath}/pages/register.jsp">Register</a>
            </button>
        </div>
    </form>
</main>
<jsp:include page="/pages/components/footer.jsp"/>
