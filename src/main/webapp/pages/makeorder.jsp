<%@ page import="uz.jasur.cafeservlet.dao.impl.MealDaoImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Meal" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Login Page"/>
</jsp:include>
<c:if test="${auth == null}">
    <c:redirect url = "login.jsp"/>
</c:if>
<%
    MealDaoImpl mealDao = new MealDaoImpl();
    List<Meal> meals = mealDao.findAll();
    request.setAttribute("meals", meals);
%>
<main class="container">
    <h3 class="text-center">${welcome_msg}</h3>
    <form class="form" action="${pageContext.request.contextPath}/controller">
        <div>
            <input type="hidden" name="command" value="make_order"/>
            <div class="mb-3">
                <label class="form-label" for="date">Enter the date: </label>
                <input class="form-control" id="date" type="date" name="date" value="" required/>
            </div>
            <div class="mb-3">
                <label class="form-label" for="time">Enter the time: </label>
                <input class="form-control" id="time" type="time" name="time" value="" required/>
            </div>
            <div class="mb-3">
                <label class="form-label" for="meals">Choose a meal:</label>
                <select class="form-select" name="meal" id="meals">
                    <option selected>Select meal</option>
                    <c:forEach items="${meals}" var="meal">
                        <option value="${meal.getName()}price:${meal.getPrice()}"> ${meal.getName()} </option>
                    </c:forEach>
                </select>
            </div>
            <div class="mb-3">
                <label class="form-label" for="amount">Enter the amount:</label>
                <input class="form-select" type="number" name="amount" id="amount" required/>
            </div>
            <div class="radios mb-3">
                <div class="form-check">
                    <input onclick="getValue(this.value)" class="form-check-input" type="radio" value="cash"
                           name="payment" id="cash">
                    <label class="form-check-label" for="cash"> Cash </label>
                </div>
                <div class="form-check">
                    <input onclick="getValue(this.value)" class="form-check-input" value="account" type="radio"
                           name="payment" id="account">
                    <label class="form-check-label" for="account"> Account </label>
                </div>
            </div>
            <button class="btn btn-outline-primary" id="preorder" data-bs-toggle="modal" data-bs-target="#modal">Submit</button>
        </div>
        <div class="modal fade" id="modal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Order confirmation</h1>
                    </div>
                    <div class="modal-body">
                        <div class="d-flex">
                            <p class="px-1">Due date for your order: </p>
                            <p id="dueDate"></p>
                        </div>
                        <div class="d-flex">
                            <p class="px-1">Your ordered meal: </p>
                            <p id="meal"></p>
                        </div>
                        <div class="d-flex">
                            <p class="px-1">Amount of your order: </p>
                            <p id="quantity"></p>
                        </div>
                        <div class="d-flex">
                            <p class="px-1">Payment type: </p>
                            <p id="paymentType"></p>
                        </div>
                        <div class="d-flex">
                            <p class="px-1">Possible loyalty bonus: </p>
                            <p id="loyalty"></p>
                        </div>
                        <div class="d-flex">
                            <p class="px-1">Total price: </p>
                            <p id="total"></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="cancel" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>
<script>
    let preorder = document.getElementById("preorder");
    let modal = document.getElementById("modal");

    function getValue(payment) {
        document.getElementById("paymentType").innerHTML = payment;
    }

    preorder.onclick = function (e) {
        e.preventDefault();
        document.getElementById("dueDate").innerHTML = document.getElementById("date").value + " " + document.getElementById("time").value;
        let mealAndPrice = document.getElementById("meals").value;
        let index = mealAndPrice.indexOf("price");
        document.getElementById("meal").innerHTML = mealAndPrice.substring(0, index);
        let amount = parseInt(document.getElementById("amount").value);
        document.getElementById("quantity").innerHTML = "" + amount;
        let price = parseInt(mealAndPrice.substring(index + 6));
        document.getElementById("total").innerHTML = "" + (price * amount);
        document.getElementById("loyalty").innerHTML = "" + (price * amount * 0.01);
    }
</script>
<jsp:include page="/pages/components/footer.jsp"/>
