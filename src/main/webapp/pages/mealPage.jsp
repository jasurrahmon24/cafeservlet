<%@ page import="uz.jasur.cafeservlet.service.MealService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.MealServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Meal" %>
<%@ page import="uz.jasur.cafeservlet.service.CommentService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.CommentServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Comment" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Meal Page"/>
</jsp:include>
<c:if test="${auth == null}">
    <c:redirect url="login.jsp"/>
</c:if>

<%
    int mealId = Integer.parseInt(request.getParameter("id"));
//    MealService mealService = new MealServiceImpl();
    CommentService commentService = new CommentServiceImpl();
    List<Comment> comments = commentService.findByMealId(mealId);
//    Meal meal = mealService.findById(mealId);
//    request.setAttribute("pageMeal", meal);
    request.setAttribute("comments", comments);
%>

<main class="container">
    <div>
<%--        <div class="mealInfo d-flex justify-content-between">--%>
<%--            <div class="card flex-shrink-0 card-style col-lg-5 p-5 mb-5">--%>
<%--                <div class="img">--%>
<%--                    <img src="${pageContext.request.contextPath}${pageMeal.getFilePath()}" class="card-img-top"--%>
<%--                         alt="${pageMeal.getName()}">--%>
<%--                </div>--%>
<%--                <div class="card-body">--%>
<%--                    <h5 class="card-title">${pageMeal.getName()} </h5>--%>
<%--                    <div class="btn d-flex align-items-center btn-light mb-2 me-3">--%>
<%--                        <h6 class="text-muted mb-0">Description: </h6>--%>
<%--                        <p class="card-text px-2">${pageMeal.getDescription()}</p>--%>
<%--                    </div>--%>
<%--                    <div class="btn d-flex align-items-center btn-light mb-2 me-3">--%>
<%--                        <h6 class="text-muted mb-0">Ingredients: </h6>--%>
<%--                        <p class="card-text px-2">${pageMeal.getIngredients()}</p>--%>
<%--                    </div>--%>
<%--                    <div class="btn d-flex align-items-center btn-light mb-2 me-3">--%>
<%--                        <h6 class="text-muted mb-0">Meal rate: </h6>--%>
<%--                        <p class="card-text px-2">${pageMeal.getOrderRate()}</p>--%>
<%--                    </div>--%>
<%--                    <div class="btn d-flex align-items-center btn-light mb-2 me-3">--%>
<%--                        <h6 class="text-muted mb-0">Price: </h6>--%>
<%--                        <p class="card-text px-2">${pageMeal.getPrice()}</p>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--            <form class="p-4 col-lg-5 flex-shrink-0 bg-light rounded mb-5"--%>
<%--                  action="${pageContext.request.contextPath}/controller">--%>
<%--                <div>--%>
<%--                    <input type="hidden" name="command" value="make_order"/>--%>
<%--                    <div class="mb-3">--%>
<%--                        <label class="form-label" for="date">Enter the date: </label>--%>
<%--                        <input class="form-control" id="date" type="date" name="date" value="" required/>--%>
<%--                    </div>--%>
<%--                    <div class="mb-3">--%>
<%--                        <label class="form-label" for="time">Enter the time: </label>--%>
<%--                        <input class="form-control" id="time" type="time" name="time" value="" required/>--%>
<%--                    </div>--%>
<%--                    <input type="hidden" value="${pageMeal.getName()}" name="meal"/>--%>
<%--                    <div class="mb-3">--%>
<%--                        <label class="form-label" for="amount">Enter the amount:</label>--%>
<%--                        <input class="form-select" type="number" name="amount" id="amount" required/>--%>
<%--                    </div>--%>
<%--                    <div class="radios mb-3">--%>
<%--                        <label class="form-label">Choose payment type:</label>--%>
<%--                        <div class="d-flex">--%>
<%--                            <div class="form-check me-3">--%>
<%--                                <input onclick="getValue(this.value)" class="form-check-input" type="radio" value="cash"--%>
<%--                                       name="payment" id="cash">--%>
<%--                                <label class="form-check-label" for="cash"> Cash </label>--%>
<%--                            </div>--%>
<%--                            <div class="form-check">--%>
<%--                                <input onclick="getValue(this.value)" class="form-check-input" value="account" type="radio"--%>
<%--                                       name="payment" id="account">--%>
<%--                                <label class="form-check-label" for="account"> Account </label>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                    <button class="btn btn-outline-primary w-25" id="preorder" data-bs-toggle="modal"--%>
<%--                            data-bs-target="#modal">--%>
<%--                        Submit--%>
<%--                    </button>--%>
<%--                </div>--%>
<%--                <div class="modal fade" id="modal">--%>
<%--                    <div class="modal-dialog modal-dialog-centered">--%>
<%--                        <div class="modal-content">--%>
<%--                            <div class="modal-header">--%>
<%--                                <h1 class="modal-title fs-5" id="exampleModalLabel">Order confirmation</h1>--%>
<%--                            </div>--%>
<%--                            <div class="modal-body">--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Due date for your order: </p>--%>
<%--                                    <p id="dueDate"></p>--%>
<%--                                </div>--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Your ordered meal: </p>--%>
<%--                                    <p id="meal"></p>--%>
<%--                                </div>--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Amount of your order: </p>--%>
<%--                                    <p id="quantity"></p>--%>
<%--                                </div>--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Payment type: </p>--%>
<%--                                    <p id="paymentType"></p>--%>
<%--                                </div>--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Possible loyalty bonus: </p>--%>
<%--                                    <p id="loyalty"></p>--%>
<%--                                </div>--%>
<%--                                <div class="d-flex">--%>
<%--                                    <p class="px-1">Total price: </p>--%>
<%--                                    <p id="total"></p>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                            <div class="modal-footer">--%>
<%--                                <button type="button" id="cancel" class="btn btn-secondary" data-bs-dismiss="modal">Cancel--%>
<%--                                </button>--%>
<%--                                <button type="submit" class="btn btn-primary">Confirm</button>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </form>--%>
<%--        </div>--%>
    </div>

    <div class="comments d-flex justify-content-center flex-wrap">
        <c:forEach items="${comments}" var="comment">
            <c:if test="${comments == null}">
                <h1 class="text-center">Nothing to show here</h1>
            </c:if>
            <div class="card mb-3 col-lg-5 mx-2">
                <div class="card-header">
                    Order id: ${comment.getOrderId()}
                </div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p>Text: ${comment.getCommentText()}</p>
                        <p>Rate: ${comment.getCommentRate()}</p>
                    </blockquote>
                </div>
            </div>
        </c:forEach>
    </div>
</main>

<script>
    let preorder = document.getElementById("preorder");
    let modal = document.getElementById("modal");

    function getValue(payment) {
        document.getElementById("paymentType").innerHTML = payment;
    }

    preorder.onclick = function (e) {
        e.preventDefault();
        document.getElementById("dueDate").innerHTML = document.getElementById("date").value + " " + document.getElementById("time").value;
        let mealAndPrice = document.getElementById("meals").value;
        let index = mealAndPrice.indexOf("price");
        document.getElementById("meal").innerHTML = mealAndPrice.substring(0, index);
        let amount = parseInt(document.getElementById("amount").value);
        document.getElementById("quantity").innerHTML = "" + amount;
        let price = parseInt(mealAndPrice.substring(index + 6));
        document.getElementById("total").innerHTML = "" + (price * amount);
        document.getElementById("loyalty").innerHTML = "" + (price * amount * 0.01);
    }
</script>

<jsp:include page="/pages/components/footer.jsp"/>