<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="Login Page"/>
</jsp:include>
<main class="container">
    <h3 class="text-center text-danger">${login_msg}</h3>
    <form class="form" action="${pageContext.request.contextPath}/controller">
        <input type="hidden" name="command" required value="register"/>
        <div class="mb-3">
            <label class="form-label" for="regLogin">Login: </label>
            <input class="form-control" id="regLogin" required type="text" name="regLogin" value=""/>
        </div>
        <div class="mb-3">
            <label class="form-label" for="fullName">FullName: </label>
            <input class="form-control" id="fullName" required type="text" name="fullName" value=""/>
        </div>
        <div class="mb-3">
            <label class="form-label" for="password">Password: </label>
            <input class="form-control" id="password" required type="password" name="password" value=""/>
        </div>
        <div class="mb-3">
            <label class="form-label" for="email">Email: </label>
            <input class="form-control" id="email" required type="email" name="email" value=""/>
        </div>
        <div class="mb-3">
            <label class="form-label" for="phoneNumber">PhoneNumber: </label>
            <input class="form-control" id="phoneNumber" required type="text" name="phoneNumber" value=""/>
        </div>
        <button type="submit" class="btn btn-outline-primary mt-2">Register</button>
    </form>
</main>
<jsp:include page="/pages/components/footer.jsp"/>
