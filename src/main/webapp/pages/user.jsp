<%@ page import="uz.jasur.cafeservlet.service.OrderService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.OrderServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.User" %>
<%@ page import="java.util.List" %>
<%@ page import="uz.jasur.cafeservlet.entity.dto.OrderMealDto" %>
<%@ page import="uz.jasur.cafeservlet.dao.impl.MealDaoImpl" %>
<%@ page import="uz.jasur.cafeservlet.service.BalanceService" %>
<%@ page import="uz.jasur.cafeservlet.service.impl.BalanceServiceImpl" %>
<%@ page import="uz.jasur.cafeservlet.entity.Balance" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/pages/components/header.jsp">
    <jsp:param name="title" value="User Cabinet Page"/>
</jsp:include>
<c:if test="${auth == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<c:if test="${auth.getUserRole().getRole() eq 'admin'}">
    <c:redirect url="admin.jsp"/>
</c:if>
<%
    OrderService orderService = new OrderServiceImpl();
    User auth = (User) request.getSession().getAttribute("auth");
    int userId = auth.getId();
    List<OrderMealDto> orders = orderService.findOrdersMealsByUserId(userId);
    BalanceService balanceService = new BalanceServiceImpl();
    Balance balance = balanceService.findByUserId(userId);
    request.setAttribute("orders", orders);
    request.setAttribute("balance", balance);
%>
<main class="container">
    <div class="d-flex flex-grow-1 mt-4">
        <div class="nav flex-column col-lg-3 nav-pills pe-3" id="v-pills-tab" role="tablist"
             aria-orientation="vertical">
            <button class="nav-link w-100 mb-4 active" id="v-pills-home-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home"
                    aria-selected="true">Orders
            </button>
            <button class="nav-link w-100 mb-4" id="v-pills-profile-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile"
                    aria-selected="false">Profile
            </button>
            <button class="nav-link w-100 mb-4" id="v-pills-messages-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages"
                    aria-selected="false">Balance & Bonuses
            </button>
        </div>

        <div class="tab-content col-lg-9 " id="v-pills-tabContent">

            <div class="tab-pane fade show active orders" id="v-pills-home" role="tabpanel"
                 aria-labelledby="v-pills-home-tab" tabindex="0">
                <c:if test="${empty orders}">
                    <h3 class="text-center">So far you have not ordered anything</h3>
                    <a href="${pageContext.request.contextPath}/pages/makeorder.jsp" class="d-block mx-auto btn btn-outline-primary w-25 mt-3">Order</a>
                </c:if>
                <c:forEach items="${orders}" var="order">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h4 class="card-title">${order.getMealName()}</h4>
                                    <div class="card-info d-flex flex-wrap mb-1">
                                        <div class="btn d-flex align-items-center btn-light mb-2 me-3">
                                            <h6 class="text-muted mb-0">Due date: </h6>
                                            <p class="card-text px-2">${order.getDueDate()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light  align-items-center mb-2 me-3">
                                            <h6 class="text-muted mb-0">Total cost: </h6>
                                            <p class="card-text px-2">${order.getTotalCost()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3  align-items-center">
                                            <h6 class="text-muted mb-0">Quantity: </h6>
                                            <p class="card-text px-2">${order.getQuantity()}</p>
                                        </div>
                                        <div class="btn d-flex btn-light mb-2 me-3 align-items-center">
                                            <h6 class="text-muted mb-0">Pay status: </h6>
                                            <p class="card-text px-2">${order.getPayStatus()}</p>
                                        </div>
                                    </div>
                                    <%
                                        MealDaoImpl dao = new MealDaoImpl();
                                    %>
                                    <button data-bs-toggle="modal" id="orderRate" data-bs-target="#orderModal"
                                            type="button" onclick="getIds(this)"
                                            class="btn btn-outline-primary w-50" data-order-id="${order.getOrderId()}"
                                            data-meal-id="${order.getMealId()}">Rate order
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex justify-content-end">
                                <img src="${pageContext.request.contextPath}${order.getImage()}"
                                     class="img-fluid rounded-end h-100" alt="${order.getMealName()}">
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>

            <div class="tab-pane fade profile" id="v-pills-profile" role="tabpanel"
                 aria-labelledby="v-pills-profile-tab" tabindex="0">
                <h3 class="text-center">${update_msg}</h3>
                <form class="form mt-0" action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="update_user"/>
                    <div class="mb-3">
                        <label class="form-label" for="regLogin">Change login: </label>
                        <input class="form-control" id="regLogin" required type="text" name="regLogin"
                               value="${auth.getLogin()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="fullName">Change FullName: </label>
                        <input class="form-control" id="fullName" required type="text" name="fullName"
                               value="${auth.getFullName()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="password">Change Password: </label>
                        <input class="form-control" id="password" required type="password" name="password"
                               value="${auth.getPassword()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="email">Change Email: </label>
                        <input class="form-control" id="email" required type="email" name="email"
                               value="${auth.getEmail()}"/>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="phoneNumber">Change PhoneNumber: </label>
                        <input class="form-control" id="phoneNumber" required type="text" name="phoneNumber"
                               value="${auth.getPhoneNumber()}"/>
                    </div>
                    <button type="submit" class="btn btn-outline-primary mt-2">Update</button>
                </form>
            </div>

            <div class="tab-pane fade balance" id="v-pills-messages" role="tabpanel"
                 aria-labelledby="v-pills-messages-tab" tabindex="0">
                <h3 class="text-center">${transfer_msg}</h3>
                <div class="card text-center mb-3 w-100">
                    <div class="card-header">
                        Balance
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Your cash balance</h5>
                        <p class="card-text">With your cash balance you can pay for meal by your accound</p>
                        <button class="btn btn-primary">${balance.getCashBalance()}</button>
                    </div>
                </div>
                <div class="card text-center mb-3 w-100">
                    <div class="card-header">
                        Bonuses
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Loyalty bonuses for you</h5>
                        <p class="card-text">These bonuses are provided when you order from our cafe.</p>
                        <a href="#" class="btn btn-primary">${balance.getLoyaltyBalance()}</a>
                    </div>
                </div>
                <div class="card text-center mb-3 w-100">
                    <div class="card-header">
                        Deposit or Withdrawal
                    </div>
                    <div class="card-body">
                        <form action="${pageContext.request.contextPath}/controller">
                            <input type="hidden" name="command" value="transfer"/>
                            <div class="mb-3  d-flex justify-content-between">
                                <div class="col-lg-8">
                                    <input class="form-control" placeholder="Enter card number" id="cardNum"
                                           type="number" name="cardNum" value=""/>
                                </div>
                                <div class="col-lg-4">
                                    <input class="form-control" placeholder="Enter amount" id="amount" required
                                           type="number" name="amount" value=""/>
                                </div>
                            </div>
                            <div class="mb-3 d-flex justify-content-between">
                                <div class="col-lg-9">
                                    <input class="form-control" placeholder="Enter card holder name" id="cardHolder"
                                           type="text" name="cardHolder" value=""/>
                                </div>
                                <div class="col-lg-3">
                                    <input class="form-control" placeholder="Enter ccv number" id="ccv" type="text"
                                           name="ccv" value=""/>
                                </div>
                            </div>
                            <div class="d-flex mb-3">
                                <div class="form-check me-3">
                                    <input class="form-check-input" type="radio" value="deposit"
                                           name="transfer" id="deposit">
                                    <label class="form-check-label" for="deposit"> Deposit </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" value="withdrawal" type="radio"
                                           name="transfer" id="withdrawal">
                                    <label class="form-check-label" for="withdrawal"> Withdrawal </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-2 w-50">Transfer</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="orderModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Rate order</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                </div>
                <form action="${pageContext.request.contextPath}/controller">
                    <input type="hidden" name="command" value="make_comment"/>
                    <input type="hidden" id="mealId" name="mealId" value=""/>
                    <input type="hidden" id="orderId" name="orderId" value=""/>
                    <div class="modal-body mx-2">
                        <div class="row mb-3">
                            <select class="form-select" aria-label="Default select example" name="commentRate" required>
                                <option value="5">Five</option>
                                <option value="4">Four</option>
                                <option value="3">Three</option>
                                <option value="2">Two</option>
                                <option value="1">One</option>
                            </select>
                        </div>
                        <div class="row mb-3">
                            <textarea class="form-control" name="commentText" placeholder="Leave a comment here"
                                      id="floatingTextarea" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<script>
    function getIds(id) {
        let mealId = id.getAttribute("data-meal-id");
        let orderId = id.getAttribute("data-order-id");
        console.log(mealId);
        console.log(orderId);
        document.getElementById("mealId").value = mealId;
        document.getElementById("orderId").value = orderId;
    }
</script>
<jsp:include page="/pages/components/footer.jsp"/>