package uz.jasur.cafeservlet.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.jasur.cafeservlet.dao.impl.BalanceDaoImpl;
import uz.jasur.cafeservlet.entity.Balance;
import uz.jasur.cafeservlet.entity.User;
import uz.jasur.cafeservlet.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class BalanceDaoTest {
    @Mock
    private Connection connection;
    @Mock
    ConnectionPool connectionPool;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private ResultSet resultSet;
    @InjectMocks
    private BalanceDaoImpl balanceDao;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.openMocks(this);
    }
    @Test
    void test_insert() throws SQLException
    {
        when(connectionPool.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        Random random = new Random();
        Balance balance = new Balance();
        balance.setId(random.nextInt());
        User user = new User();
        user.setId(13);
        balance.setUser(user);
        balance.setCashBalance(random.nextDouble());
        balance.setLoyaltyBalance(random.nextDouble());
        boolean insert = balanceDao.insert(balance);
        assertTrue(insert);

    }
}
