package uz.jasur.cafeservlet.pool;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConnectionPoolTest {
    private ConnectionPool connectionPool;

    @BeforeEach
    public void init() {
        connectionPool = ConnectionPool.getInstance();
    }

    @Test
    public void getConnection() {
        Connection connection = connectionPool.getConnection();
        assertNotNull(connection);
        connectionPool.releaseConnection(connection);
    }

    @Test
    public void releaseConnection() {
        Connection connection = connectionPool.getConnection();
        boolean actual = connectionPool.releaseConnection(connection);
        assertTrue(actual);
    }
}
